<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class CreatePostController extends Controller
{
    public function index()
    {
        return view('createPost');
    }

    public function showPosts()
    {
        $posts = Post::get();

        return view('showPosts', [
            'posts' => $posts
        ]);
    }

    public function showPostInfo($id)
    {
        $post = Post::find($id);

        $post->update([
            'title' => $post->title,
            'body' => $post->body,
        ]);

        return view('showPostInformation', [
            'post' => $post
        ]);
    }

    public function updatePost(Request $request, $id)
    {
        $post = Post::find($id);

        $this->validate($request, [
            'title' => 'required',
            'body' => 'required',
        ]);

        $post->update([
            'title' => $request->title,
            'body' => $request->body,
        ]);

        return redirect()->route('showPosts');
    }

    public function deletePost()
    {
        $posts = Post::get();
        foreach ($posts as $post) {
            $post = Post::where('id', '=', $post->id);
        }
        $post->delete();
        return redirect()->route('showPosts');
    }

    public function storePostData(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|unique:posts,title',
            'body' => 'required',
        ]);

        Post::create([
            'title' => $request->title,
            'body' => $request->body,
        ]);

        return redirect()->route('showPosts');
    }
}
