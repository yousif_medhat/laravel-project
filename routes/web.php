<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CreatePostController;

Route::get('/', function () {
    return view('layouts.app');
});

Route::controller(CreatePostController::class)->group(function () {
    Route::get('/createPost', 'index')->name('createPost');
    Route::post('/createPost', 'storePostData');
    Route::get('/showPosts', 'showPosts')->name('showPosts');
    Route::get('/showPosts/{id}', 'showPostInfo')->name('showPostInfo');
    Route::post('/showPosts/{id}', 'updatePost');
    Route::get('/deletePost', 'deletePost')->name('deletePost');
});
