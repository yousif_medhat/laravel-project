<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Posts</title>
    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
    <script src="{{url('js/main.js')}}"></script>
</head>

<body class="bg-gray-300">
    <div class="w-full bg-red-200 p-10 text-center text-gray-600 font-bold text-2xl border-b-2 border-white">
        You Can Create Your Own Post In This Place ..!
    </div>
    <div class="flex flex-col justify-center" style="height: 80vh;">
        <div class="flex justify-center">
            <div class="flex justify-between w-1/4">
                <a href="{{ route('createPost') }}" class="bg-red-200 px-10 py-1.5 rounded-full text-xl text-gray-600 font-bold border-2 border-white">
                    Create post
                </a>

                <a href="{{ route('showPosts') }}" class="bg-red-200 px-10 py-1.5 rounded-full text-xl text-gray-600 font-bold border-2 border-white">
                    Show Posts
                </a>
            </div>
        </div>

        <div class="flex justify-center my-10">
            @yield('content')
        </div>
    </div>


</body>

</html>