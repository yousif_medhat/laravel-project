@extends('layouts.app')



@section('content')
<div class="w-1/4">
    <p class="text-center text-2xl font-bold text-gray-600">List of all your posts</p>
    <div class="p-8 my-10 border-2 border-gray-600 rounded-lg bg-gray-50">
        @if(count($posts))
        @foreach($posts as $post)
        <div class="my-1.5 flex justify-between items-center border-b-2 border-gray-600 pb-1.5">
            <p class="text-lg font-bold text-gray-600">{{ $post-> title }}</p>
            <div>
                <a href="{{ route('showPostInfo', $post->id) }}" class="bg-gray-600 py-1 px-5 text-white font-bold rounded-full">Show post info</a>
                <a href="{{ route('deletePost') }}" class="bg-red-400 py-1 px-5 text-white font-bold rounded-full">Delete</a>
            </div>
        </div>
        @endforeach
        @else
        <div class="text-center text-gray-700 font-bold text-lg">
            Sorry, There is no posts to show right now ..!
        </div>
        @endif
    </div>

</div>
@endsection