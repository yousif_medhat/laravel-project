@extends('layouts.app')

@section('content')
<div class="w-1/4">
    <form action="{{ route('createPost') }}" method="post" class="space-y-2.5">
        @csrf
        <div>
            <input name="title" id="title" class="border-2 p-4 rounded-xl w-full focus:outline-none focus:border-blue-500 @if($errors->has('body')) border-2 border-red-500 @endif" placeholder="Title" value="{{ old('title') }}">
            @error('title')
            <div class="ml-5 mt-0.5 text-red-500">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div>
            <input name="body" id="body" autocomplete="false" class="border-2 p-4 rounded-xl w-full focus:outline-none focus:border-blue-500 @if($errors->has('body')) border-2 border-red-500 @endif" placeholder="Body" value="{{ old('body') }}">
            @error('body')
            <div class="ml-5 mt-0.5 text-red-500">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div>
            <button type="submit" class="p-5 text-gray-600 font-bold bg-red-200 rounded-xl w-full border-2 border-white">CREATE POST</button>
        </div>
    </form>
</div>
@endsection