@extends('layouts.app')


@section('content')
<div class="w-1/4 space-y-5">
    <p class="text-center text-2xl font-bold text-gray-600">Your Post Information</p>
    <form action="{{ route('showPostInfo', $post->id) }}" method="post">
        <div class="flex flex-col space-y-3.5">
            @csrf
            <input name="title" id="title" value="{{$post->title}}" class="border-2 p-4 rounded-xl w-full focus:outline-none focus:border-blue-500 @if($errors->has('title')) border-2 border-red-500 @endif" />
            @error('title')
            <div class="ml-5 mt-0.5 text-red-500">
                {{ $message }}
            </div>
            @enderror
            <input name="body" id="body" value="{{$post->body}}" class="border-2 p-4 rounded-xl w-full focus:outline-none focus:border-blue-500 @if($errors->has('body')) border-2 border-red-500 @endif" />
            @error('body')
            <div class="ml-5 mt-0.5 text-red-500">
                {{ $message }}
            </div>
            @enderror
            <button id="submit" type="submit" class="p-5 text-gray-600 font-bold bg-red-200 rounded-xl w-full border-2 border-white">
                Update
            </button>
        </div>
    </form>
</div>
@endsection